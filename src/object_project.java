public class object_project {
    private String unitNo, projectName, contractNo, client, fieldLocation, pic;

    public object_project(String unitNo, String projectName, String contractNo, String client, String fieldLocation, String pic) {
        this.unitNo = unitNo;
        this.projectName = projectName;
        this.contractNo = contractNo;
        this.client = client;
        this.fieldLocation = fieldLocation;
        this.pic = pic;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getContractNo() {
        return contractNo;
    }

    public String getClient() {
        return client;
    }

    public String getFieldLocation() {
        return fieldLocation;
    }

    public String getPic() {
        return pic;
    }
}
