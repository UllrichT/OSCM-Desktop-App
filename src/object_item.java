public class object_item {
    String inventoryCode, serialNo, description, _class, pin, box, wp, stdH2s, status;
    int safetyStock, qty;

    public object_item(String inventoryCode, String serialNo, String description, int safetyStock, int qty, String _class, String pin, String box, String wp, String stdH2s, String status) {
        this.inventoryCode = inventoryCode;
        this.serialNo = serialNo;
        this.description = description;
        this._class = _class;
        this.pin = pin;
        this.box = box;
        this.wp = wp;
        this.stdH2s = stdH2s;
        this.status = status;
        this.safetyStock = safetyStock;
        this.qty = qty;
    }

    public String getInventoryCode() {
        return inventoryCode;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public String getDescription() {
        return description;
    }

    public String get_class() {
        return _class;
    }

    public String getPin() {
        return pin;
    }

    public String getBox() {
        return box;
    }

    public String getWp() {
        return wp;
    }

    public String getStdH2s() {
        return stdH2s;
    }

    public String getStatus() {
        return status;
    }

    public String getSafetyStock() {
        return String.valueOf(safetyStock);
    }

    public String getQty() {
        return String.valueOf(qty);
    }
}
