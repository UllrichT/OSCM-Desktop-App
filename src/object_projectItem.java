public class object_projectItem {
    String serialNumber;
    int qty;

    public object_projectItem(String serialNumber, int qty) {
        this.serialNumber = serialNumber;
        this.qty = qty;
    }

    public String getSerialNumber() { return serialNumber; }

    public int getQty() {
        return qty;
    }
}
