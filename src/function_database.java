import java.sql.*;

public class function_database {
    public static void tryToConnect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            System.out.println("successfully connected to the database");
        }
        catch (Exception e) {
            System.out.println("failed to connect to database");
        }
    }
    public static void addItem(object_item newItem) {
        String query = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "INSERT INTO `ms_item` (`inventoryCode`, `serialNumber`, `description`, `safetyStock`, `class`, `pin`, `box`, `wp`, `stdH2s`, `status`) \n" +
                    "VALUES ('" + newItem.inventoryCode + "', '" + newItem.serialNo + "', '" + newItem.description + "', '" + newItem.safetyStock + "', '" + newItem._class + "', '" + newItem.pin + "', '" + newItem.box + "', '" + newItem.wp + "', '" + newItem.stdH2s + "', '" + newItem.status + "');";
            stm.executeUpdate(query);
            query = "INSERT INTO `ms_item_qty`(`projectName`, `serialNumber`, `qty`) \n" +
                    "VALUES ('item_master', '" + newItem.getSerialNo() + "', '" + newItem.getQty() + "')";
            stm.executeUpdate(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func addItem : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
    }
    public static void addProject(object_project newProject) {
        String query = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "INSERT INTO `ms_projects` (`unitNo`, `projectName`, `contractNo`, `client`, `fieldLocation`, `pic`)\n"
                    + "VALUES ('" + newProject.getUnitNo() + "', '" + newProject.getProjectName() + "', '" + newProject.getContractNo() + "', '" + newProject.getClient() + "', '" + newProject.getFieldLocation() + "', '" + newProject.getPic() + "');";
            stm.executeUpdate(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func addProject : " + e.getCause());
            System.out.println("Query : \n" + query);
        }
    }
    public static void updateItem(object_item item, String oldSerialNo) {
        String query = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "UPDATE `ms_item` SET \n" +
                    "`inventoryCode`='" + item.getInventoryCode() + "',\n" +
                    "`serialNumber`='" + item.getSerialNo() + "',\n" +
                    "`description`='" + item.getDescription() + "',\n" +
                    "`safetyStock`='" + item.getSafetyStock() + "',\n" +
                    "`class`='" + item.get_class() + "',\n" +
                    "`pin`='" + item.getPin() + "',\n" +
                    "`box`='" + item.getBox() + "',\n" +
                    "`wp`='" + item.getWp() + "',\n" +
                    "`stdH2s`='" + item.getStdH2s() + "',\n" +
                    "`status`='" + item.getStatus() + "' \n" +
                    "WHERE serialNumber = '" + oldSerialNo + "'";
            stm.executeUpdate(query);
        }
        catch (Exception e)
        {
            System.out.println("\nError at func updateItem : " + e.getCause());
            System.out.println("Query : \n" + query);
        }
    }
    public static void updateItemStock(String projectName, String serialNumber, int newQty) {
        String query = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "UPDATE `ms_item_qty`\n" +
                    "SET `qty`='" + newQty + "'\n" +
                    "WHERE serialNumber = '" + serialNumber + "' AND projectName = '" + projectName + "'";
            stm.executeUpdate(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func updateItemStock : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
    }
    public static void moveItem(String projectName, object_projectItem item, int itemMaster_qty) {
        String query = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "SELECT * \n" +
                    "FROM `ms_item_qty` \n" +
                    "WHERE projectName = '" + projectName + "' AND serialNumber = '" + item.getSerialNumber() + "'";
            ResultSet result = stm.executeQuery(query);
            result.last();
            boolean itemAvailable = true;
            if(result.getRow() == 0) itemAvailable = false;
            if(itemAvailable) {
//                item sudah ada di project, jadi tinggal update add qty di table namaProject
                int itemProject_qty = result.getInt(3) + item.getQty();
                updateItemStock(projectName, item.getSerialNumber(), itemProject_qty);
            }
            else {
//                item blm ada di project, jadi harus insert item di table namaProject
                query = "INSERT INTO `ms_item_qty`(`projectName`, `serialNumber`, `qty`) \n" +
                        "VALUES ('" + projectName + "','" + item.getSerialNumber() + "','" + item.getQty() +"')";
                stm.executeUpdate(query);
            }

//            add movement detail ke table item_movement
            addItemMovement("item_master", projectName, item.getSerialNumber(), item.getQty());

//            update stock itemMaster
            int updatedItemMasterQty = itemMaster_qty - item.getQty();
            updateItemStock("item_master", item.getSerialNumber(), updatedItemMasterQty);
        }
        catch (Exception e) {
            System.out.println("\nError at func moveItem : " + e.getCause());
            System.out.println("Query : \n" + query);
        }
    }
    public static void addItemMovement(String moveFrom, String moveTo, String serialNumber, int qty) {
        String query = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "INSERT INTO `ms_item_movement` (`date`, `moveFrom`, `moveTo`, `serialNo`, `qty`) \n" +
                    "VALUES (CURRENT_DATE(), '" + moveFrom + "', '" + moveTo + "', '" + serialNumber + "', '" + qty + "');";
            stm.executeUpdate(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func addItemMovement : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
    }
    public static ResultSet getClassList() {
        String query = null;
        ResultSet result = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "SELECT DISTINCT class FROM `ms_item`";
            result = stm.executeQuery(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func getClassList : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
        return result;
    }
    public static ResultSet getProjectList() {
        String query = null;
        ResultSet result = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "SELECT projectName FROM `ms_projects`";
            result = stm.executeQuery(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func getProjectList : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
        return result;
    }
    public static ResultSet getAllItemList() {
        String query = null;
        ResultSet result = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "SELECT \n" +
                    "\tms_item.inventoryCode, \n" +
                    "    ms_item.serialNumber, \n" +
                    "    ms_item.description, \n" +
                    "    ms_item.safetyStock, \n" +
                    "    ms_item_qty.qty, \n" +
                    "    ms_item.class, \n" +
                    "    ms_item.pin, \n" +
                    "    ms_item.box, \n" +
                    "    ms_item.wp, \n" +
                    "    ms_item.stdH2s, \n" +
                    "    ms_item.status\n" +
                    "FROM `ms_item`, `ms_item_qty`\n" +
                    "WHERE ms_item.serialNumber = ms_item_qty.serialNumber AND ms_item_qty.projectName = 'item_master'";
            result = stm.executeQuery(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func getAllItemList : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
        return result;
    }
    public static ResultSet getProjectItem(String projectName) {
        String query = null;
        ResultSet result = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "SELECT \n" +
                    "\tms_item.inventoryCode,\n" +
                    "\tms_item.serialNumber,\n" +
                    "\tms_item.description,\n" +
                    "\tms_item_qty.qty,\n" +
                    "\tms_item.class,\n" +
                    "\tms_item.pin,\n" +
                    "\tms_item.box,\n" +
                    "\tms_item.wp\n" +
                    "FROM `ms_item`, `ms_item_qty`\n" +
                    "WHERE ms_item.serialNumber = ms_item_qty.serialNumber AND ms_item_qty.projectName = '" + projectName + "'";
            result = stm.executeQuery(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func getProjectItem : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
        return result;
    }
    public static ResultSet getItemMovement(String serialNumber, String projectName) {
        String query = null;
        ResultSet result = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "SELECT * \n" +
                    "FROM `ms_item_movement` \n" +
                    "WHERE (moveFrom = '" + projectName + "' OR moveTo = '" + projectName + "') AND serialNo = '" + serialNumber + "'";
            result = stm.executeQuery(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func getItemMovement : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
        return result;
    }
    public static ResultSet getItemTemplate(String inventoryCode) {
        String query = null;
        ResultSet result = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "SELECT *\n" +
                    "FROM `ms_item`\n" +
                    "WHERE inventoryCode = '" + inventoryCode + "'";
            result = stm.executeQuery(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func getItemTemplate : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
        return result;
    }
    public static ResultSet getSearchItemList(String inventoryCode, String description, String _class, String projectName) {
        String query = null;
        ResultSet result = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/oscm","root","");
            Statement stm = con.createStatement();
            query = "SELECT \n" +
                    "\tms_item.inventoryCode, \n" +
                    "    ms_item.serialNumber, \n" +
                    "    ms_item.description, \n" +
                    "    ms_item.safetyStock, \n" +
                    "    ms_item_qty.qty, \n" +
                    "    ms_item.class, \n" +
                    "    ms_item.pin, \n" +
                    "    ms_item.box, \n" +
                    "    ms_item.wp, \n" +
                    "    ms_item.stdH2s, \n" +
                    "    ms_item.status\n" +
                    "FROM `ms_item`, `ms_item_qty`\n" +
                    "WHERE \n" +
                    "ms_item.inventoryCode LIKE '%" + inventoryCode + "%' AND \n" +
                    "ms_item.description LIKE '%" + description + "%' AND \n" +
                    "ms_item.class LIKE '%" + _class + "%' AND\n" +
                    "ms_item_qty.projectName = '" + projectName + "' AND\n" +
                    "ms_item_qty.serialNumber = ms_item.serialNumber\n" +
                    "ORDER BY inventoryCode";
            result = stm.executeQuery(query);
        }
        catch (Exception e) {
            System.out.println("\nError at func getSearchItemList : " + e.getCause());
            System.out.println("Query :\n" + query);
        }
        return result;
    }
}