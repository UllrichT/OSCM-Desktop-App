import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.ResultSet;
import java.util.ArrayList;

public class function {
    public static void updateProjectList(DefaultListModel<String> projects) {
        try {
            projects.removeAllElements();
            ResultSet rs = function_database.getProjectList();
            while (rs.next()) {
                projects.addElement(rs.getString(1));
            }
        }
        catch (Exception e) {
            System.out.println("Error at func updateProjectList : " + e.getCause());
        }
    }
    public static void getProjectList(JComboBox cb_projectList) {
        try {
            ResultSet rs = function_database.getProjectList();
            while (rs.next())
            {
                cb_projectList.addItem(rs.getString(1));
            }
        }
        catch (Exception e) {
            System.out.println("Error at func getProjectList : " + e.getCause());
        }
    }
    public static void getClassList(JComboBox cb_classList) {
        try {
            ResultSet rs = function_database.getClassList();
            while (rs.next())
            {
                cb_classList.addItem(rs.getString(1));
            }
        }
        catch (Exception e) {
            System.out.println("Error at func getProjectList : " + e.getCause());
        }
    }
    public static void updateItemMasterList(DefaultTableModel itemList) {
        try{
            int rows = itemList.getRowCount();
            for(int row = rows - 1; row >= 0; row--) {
                itemList.removeRow(row);
            }
            ResultSet rs = function_database.getAllItemList();
            while (rs.next()) {
                itemList.addRow(new String[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11)});
            }
        }
        catch (Exception e) {
            System.out.println("Error at func updateItemMasterList : " + e.getCause());
        }
    }
    public static void searchItem(String inventoryCode, String description, String _class, DefaultTableModel searchItemList) {
        try {
            int rows = searchItemList.getRowCount();
            for(int row = rows - 1; row >= 0; row--) {
                searchItemList.removeRow(row);
            }
            ResultSet rs = function_database.getSearchItemList(inventoryCode, description, _class, "item_master");
            while (rs.next()) {
                searchItemList.addRow(new String[]{rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11)});
            }
        }
        catch (Exception e) {
            System.out.println("Error at func searchItem : " + e.getCause());
        }
    }
    public static object_item getSelectedItem(JTable tabel) {
        int row = tabel.getSelectedRow();
        String invCode = String.valueOf(tabel.getValueAt(row, 0));
        String serialNo = String.valueOf(tabel.getValueAt(row, 1));
        String desc = String.valueOf(tabel.getValueAt(row, 2));
        int safetyStock = Integer.parseInt(String.valueOf(tabel.getValueAt(row, 3)));
        int qty = Integer.parseInt(String.valueOf(tabel.getValueAt(row, 4)));
        String _class = String.valueOf(tabel.getValueAt(row, 5));
        String pin = String.valueOf(tabel.getValueAt(row, 6));
        String box = String.valueOf(tabel.getValueAt(row, 7));
        String wp = String.valueOf(tabel.getValueAt(row, 8));
        String stdH2s = String.valueOf(tabel.getValueAt(row, 9));
        String status = String.valueOf(tabel.getValueAt(row, 10));
        object_item item = new object_item(invCode, serialNo, desc, safetyStock, qty, _class, pin, box, wp, stdH2s, status);
        return item;
    }
    public static void updateItemProject(DefaultTableModel model, String projectName) {
        try {
            int rows = model.getRowCount();
            for(int row = rows - 1; row >= 0; row--) {
                model.removeRow(row);
            }
            ResultSet rs = function_database.getProjectItem(projectName);
            while (rs.next()) {
                model.addRow(new String[] {rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                });
            }
        }
        catch (Exception e) {
            System.out.println("Error at func updateItemProject : " + e.getCause());
        }
    }
    public static void updateItemMovement(String serialNumber, DefaultTableModel itemMovement) {
        try {
            ResultSet rs = function_database.getItemMovement(serialNumber, "item_master");
            while (rs.next()) {
                itemMovement.addRow(new String[] {rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(5)});
            }
        }
        catch (Exception e) {
            System.out.println("Error at func updateItemMovement : " + e.getCause());
        }
    }
}
