import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;

public class MAIN {
    private static Border blackline = BorderFactory.createLineBorder(Color.black);
    private static class ColumnColorRenderer extends DefaultTableCellRenderer {
        public ColumnColorRenderer() {
            super();
        }
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,   boolean hasFocus, int row, int column) {
            Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if(value.equals("Ready")) {
                cell.setBackground(Color.green);
            }
            else {
                cell.setBackground(Color.red);
            }
            return cell;
        }
    }

    private static JPanel pnl_project(String title, JTabbedPane tabMenu) {
        JPanel newPnl = new JPanel();
        newPnl.setName(title);
        GroupLayout layout = new GroupLayout(newPnl);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        newPnl.setLayout(layout);

        String[] header = {"Inventory Code",
                "Serial no.",
                "Description",
                "Qty",
                "Class",
                "PIN",
                "BOX",
                "WP"};
        DefaultTableModel model = new DefaultTableModel(header, 0);
        JTable tabel = new JTable(model);
        tabel.setDefaultEditor(Object.class, null);
        tabel.getColumnModel().getColumn(1).setMinWidth(100);
        tabel.getColumnModel().getColumn(2).setMinWidth(150);
        tabel.getColumnModel().getColumn(3).setMaxWidth(60);
        tabel.getColumnModel().getColumn(5).setMaxWidth(70);
        tabel.getColumnModel().getColumn(6).setMaxWidth(70);
        tabel.getColumnModel().getColumn(7).setMaxWidth(70);
        JScrollPane pn = new JScrollPane(tabel);
        function.updateItemProject(model, title);

        JButton btn_close = new JButton("Close");
        JButton btn_refresh = new JButton("Refresh");
        JButton btn_searchItem = new JButton("Search item");
        JButton btn_showProjectDetail = new JButton("Show project detail");

        btn_refresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                function.updateItemProject(model, title);
            }
        });
        btn_close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Component selected = tabMenu.getSelectedComponent();
                if (selected != null) tabMenu.remove(selected);
            }
        });

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGroup(
                                layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addComponent(pn)
                                        .addGroup(
                                                layout.createSequentialGroup()
                                                        .addComponent(btn_showProjectDetail)
//                                                        .addComponent(btn_searchItem)
                                                        .addComponent(btn_refresh)
                                                        .addComponent(btn_close)
                                        )
                        )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(pn)
                        .addGroup(
                                layout.createParallelGroup()
                                        .addComponent(btn_showProjectDetail)
//                                        .addComponent(btn_searchItem)
                                        .addComponent(btn_refresh)
                                        .addComponent(btn_close)
                        )
        );

        return newPnl;
    }
    private static JPanel pnl_inventoryStatus_allocated_projectManager_createProject(JTabbedPane projectTab, DefaultListModel<String> projects) {
        JPanel newPnl = new JPanel();
        newPnl.setBorder(BorderFactory.createTitledBorder(blackline, "Create New Project"));
        newPnl.setBounds(5, 5, 290, 265);
        newPnl.setLayout(null);

        JLabel lbl_unitNo = new JLabel("Unit No");
        JLabel lbl_projectName = new JLabel("Project");
        JLabel lbl_contractNo = new JLabel("Contract No");
        JLabel lbl_client = new JLabel("Client");
        JLabel lbl_fieldLocation = new JLabel("Field Location");
        JLabel lbl_pic = new JLabel("PIC");

        JTextField tf_unitNo = new JTextField();
        JTextField tf_projectName = new JTextField();
        JTextField tf_contractNo = new JTextField();
        JTextField tf_client = new JTextField();
        JTextField tf_fieldLocation = new JTextField();
        JTextField tf_pic = new JTextField();

        JButton btn_createProject = new JButton("Create");

        lbl_unitNo.setBounds(10, 25, 100, 10);
        lbl_projectName.setBounds(10, 60, 100, 10);
        lbl_contractNo.setBounds(10, 95, 100, 10);
        lbl_client.setBounds(10, 130, 100, 10);
        lbl_fieldLocation.setBounds(10,165 , 100, 10);
        lbl_pic.setBounds(10, 200, 100, 10);

        tf_unitNo.setBounds(120, 20, 160, 25);
        tf_projectName.setBounds(120, 55, 160, 25);
        tf_contractNo.setBounds(120, 90, 160, 25);
        tf_client.setBounds(120, 125, 160, 25);
        tf_fieldLocation.setBounds(120, 160, 160, 25);
        tf_pic.setBounds(120, 195, 160, 25);

        btn_createProject.setBounds(200, 230, 79, 25);
        btn_createProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String unitNo = tf_unitNo.getText();
                String projectName = tf_projectName.getText().replace(" ", "_");
                String contractNo = tf_contractNo.getText();
                String client = tf_client.getText();
                String fieldLocaton = tf_fieldLocation.getText();
                String pic = tf_pic.getText();
                if(unitNo.length()==0 || projectName.length()==0 || contractNo.length()==0 || client.length()==0 || fieldLocaton.length()==0 || pic.length()==0) {
                    JOptionPane.showMessageDialog(newPnl, "All field cannot be empty","Error", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    object_project newProject = new object_project(unitNo, projectName, contractNo, client, fieldLocaton, pic);
                    function_database.addProject(newProject);
                    function.updateProjectList(projects);
                    projectTab.add(pnl_project(projectName, projectTab));
                    tf_unitNo.setText("");
                    tf_projectName.setText("");
                    tf_contractNo.setText("");
                    tf_client.setText("");
                    tf_fieldLocation.setText("");
                    tf_pic.setText("");
                }
            }
        });

        newPnl.add(lbl_unitNo);
        newPnl.add(lbl_projectName);
        newPnl.add(lbl_contractNo);
        newPnl.add(lbl_client);
        newPnl.add(lbl_fieldLocation);
        newPnl.add(lbl_pic);

        newPnl.add(tf_unitNo);
        newPnl.add(tf_projectName);
        newPnl.add(tf_contractNo);
        newPnl.add(tf_client);
        newPnl.add(tf_fieldLocation);
        newPnl.add(tf_pic);

        newPnl.add(btn_createProject);

        return newPnl;
    }
    private static JPanel pnl_inventoryStatus_allocated_projectManager_projectList(JTabbedPane projectTab, DefaultListModel<String> projects) {
        JPanel newPnl = new JPanel();
        newPnl.setLayout(new GridLayout(1,1));
        newPnl.setBorder(BorderFactory.createTitledBorder(blackline, "Project List"));
        newPnl.setBounds(5, 275, 290, 355);

        function.updateProjectList(projects);
        JList<String> projectList = new JList<>(projects);
        JScrollPane pn = new JScrollPane(projectList);
        newPnl.add(pn);

        projectList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JList list = (JList)e.getSource();
                if (e.getClickCount() == 2) {
                    int index = list.locationToIndex(e.getPoint());
                    String title = projectList.getModel().getElementAt(index);
                    boolean exist = false;
                    for(int tabIndex = 0; tabIndex < projectTab.getTabCount(); tabIndex++) {
                        if(projectTab.getTitleAt(tabIndex).equals(title)) {
                            exist = true;
                            break;
                        }
                    }
                    if(exist == false) {
                        projectTab.add(pnl_project(title, projectTab));
                    }
                }
            }
        });

        return newPnl;
    }
    private static JPanel pnl_inventoryStatus_allocated_projectManager(JTabbedPane projectTab) {
        JPanel newPnl = new JPanel();
        newPnl.setBorder(BorderFactory.createTitledBorder(blackline));
        newPnl.setBounds(5,5,300,637);
        newPnl.setLayout(null);

        DefaultListModel<String> projects = new DefaultListModel<>();
        JPanel pnl_createProject = pnl_inventoryStatus_allocated_projectManager_createProject(projectTab, projects);
        JPanel pnl_projectList = pnl_inventoryStatus_allocated_projectManager_projectList(projectTab, projects);

        newPnl.add(pnl_createProject);
        newPnl.add(pnl_projectList);

        return newPnl;
    }
    private static JPanel pnl_inventoryStatus_allocated() {
        JPanel mainPnl_allocated = new JPanel();
        mainPnl_allocated.setLayout(null);

        JPanel pnl_projectTab = new JPanel();
        pnl_projectTab.setBounds(310, 5, 1040, 637);
        pnl_projectTab.setLayout(new GridLayout(1, 1));
        JTabbedPane tabMenu = new JTabbedPane();

        JPanel pnl_projectManager = pnl_inventoryStatus_allocated_projectManager(tabMenu);

        pnl_projectTab.add(tabMenu);
        mainPnl_allocated.add(pnl_projectTab);
        mainPnl_allocated.add(pnl_projectManager);

        return mainPnl_allocated;
    }
    private static JPanel pnl_inventoryStatus() {
        JPanel mainPnl_2 = new JPanel();
        mainPnl_2.setLayout(new GridLayout(1,1));

        JTabbedPane tabMenu = new JTabbedPane();
        JPanel pnl1 = pnl_inventoryStatus_allocated();
        JPanel pnl2 = new JPanel();

        mainPnl_2.add(tabMenu);
        tabMenu.add("Allocated", pnl1);
        tabMenu.add("Projected Available Balance", pnl2);

        return mainPnl_2;
    }
    private static JPanel pnl_itemMaster() {
        JPanel mainPnl_1 = new JPanel();
        GroupLayout layout = new GroupLayout(mainPnl_1);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        mainPnl_1.setLayout(layout);

        String[] header = {"Inventory Code",
                "Serial no.",
                "Description",
                "Safety Stock",
                "Qty",
                "Class",
                "PIN",
                "BOX",
                "WP",
                "STD or H2S",
                "Status"};
        DefaultTableModel model = new DefaultTableModel(header, 0);
        JTable tabel = new JTable(model);
        tabel.setDefaultEditor(Object.class, null);
        tabel.getColumnModel().getColumn(1).setMinWidth(100);
        tabel.getColumnModel().getColumn(2).setMinWidth(150);
        tabel.getColumnModel().getColumn(3).setMaxWidth(80);
        tabel.getColumnModel().getColumn(4).setMaxWidth(70);
        tabel.getColumnModel().getColumn(6).setMaxWidth(70);
        tabel.getColumnModel().getColumn(7).setMaxWidth(70);
        tabel.getColumnModel().getColumn(8).setMaxWidth(70);
        tabel.getColumnModel().getColumn(9).setMaxWidth(80);
        tabel.getColumnModel().getColumn(10).setMaxWidth(80);
        JScrollPane pn = new JScrollPane(tabel);
        function.updateItemMasterList(model);

        TableColumn statusColumn = tabel.getColumnModel().getColumn(10);
        DefaultTableCellRenderer newRenderer = new ColumnColorRenderer();
        statusColumn.setCellRenderer(newRenderer);

        JButton btn_search = new JButton("Search");
        btn_search.addActionListener(actionEvent -> {
            coba.frame_searchItem(model);
        });

        JButton btn_addItem = new JButton("Add item");
        btn_addItem.addActionListener(actionEvent -> {
            coba.frame_addItem(model, null);
        });

        JButton btn_criticalItem = new JButton("Critical Item");
        btn_criticalItem.addActionListener(actionEvent -> {

        });

        JButton btn_updateItem = new JButton("Update item");
        btn_updateItem.addActionListener(actionEvent -> {
            try {
                object_item item = function.getSelectedItem(tabel);
                coba.frame_addItem(model, item);
            }
            catch (Exception e) {
                JOptionPane.showMessageDialog(mainPnl_1, "Please select an item's row", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        JButton btn_showItemMovement = new JButton("Show item movement");
        btn_showItemMovement.addActionListener(actionEvent -> {
            try {
                object_item item = function.getSelectedItem(tabel);
                coba.frame_itemMovement(item);
            }
            catch (Exception e) {
                JOptionPane.showMessageDialog(mainPnl_1, "Please select an item's row", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        JButton btn_restockItem = new JButton("Restock item");
        btn_restockItem.addActionListener(actionEvent -> {
            try {
                object_item item = function.getSelectedItem(tabel);
                coba.frame_restockItem(model, item);
            }
            catch (Exception e) {
                JOptionPane.showMessageDialog(mainPnl_1, "Please select an item's row", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        JButton btn_moveItem = new JButton("Move item");
        btn_moveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    object_item item = function.getSelectedItem(tabel);
                    coba.frame_moveItem(model, item);
                }
                catch (Exception e) {
                    JOptionPane.showMessageDialog(mainPnl_1, "Please select an item's row", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(pn)
                        .addGroup(
                                layout.createSequentialGroup()
                                .addComponent(btn_search)
                                .addComponent(btn_addItem)
                                .addComponent(btn_updateItem)
                                .addComponent(btn_restockItem)
                                .addComponent(btn_criticalItem)
                                .addComponent(btn_showItemMovement)
                                .addComponent(btn_moveItem)
                        )
                )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                .addComponent(pn)
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(btn_search)
                        .addComponent(btn_addItem)
                        .addComponent(btn_updateItem)
                        .addComponent(btn_restockItem)
                        .addComponent(btn_criticalItem)
                        .addComponent(btn_showItemMovement)
                        .addComponent(btn_moveItem)
                )
        );

        return mainPnl_1;
    }
    private static void frame_mainFrame() {
        JFrame mainFrame = new JFrame("Aplikasi OSCM");
        mainFrame.setLayout(new GridLayout(1, 1));
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        mainFrame.setVisible(true);

        JTabbedPane TabMenu = new JTabbedPane();
        JPanel tab1 = pnl_itemMaster();
        JPanel tab2 = pnl_inventoryStatus();

        mainFrame.add(TabMenu);
        TabMenu.add("Item Master Data Segment", tab1);
        TabMenu.add("Inventory Status Segment", tab2);
    }
    public static void main(String[] args) {
        System.out.println("start...");
        function_database.tryToConnect();
        frame_mainFrame();
    }
}