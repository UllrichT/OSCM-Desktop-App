import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;

public class coba {
    private static Border blackline = BorderFactory.createLineBorder(Color.black);
    public static void frame_addItem(DefaultTableModel itemList, object_item item) {
        String oldSerialNo = null;
        JFrame mainFrame = new JFrame("Add Item");
        if(item != null) {
            mainFrame.setTitle("Update Item");
        }
        mainFrame.setVisible(true);
        mainFrame.setSize(350, 390);
        mainFrame.setLayout(new GridLayout(1, 1));

        JPanel mainPnl = new JPanel();
        mainFrame.add(mainPnl);

        GroupLayout layout = new GroupLayout(mainPnl);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        mainPnl.setLayout(layout);

        JLabel lbl_invCode = new JLabel("Inventory Code");
        JLabel lbl_serialNo = new JLabel("Serial Number");
        JLabel lbl_desc = new JLabel("Description");
        JLabel lbl_safetyStock = new JLabel("Safety Stock");
        JLabel lbl_qty = new JLabel("Qty");
        JLabel lbl_class = new JLabel("Class");
        JLabel lbl_pin = new JLabel("PIN");
        JLabel lbl_box = new JLabel("BOX");
        JLabel lbl_wp = new JLabel("WP");
        JLabel lbl_stdH2S = new JLabel("STD od H2S");
        JLabel lbl_status = new JLabel("Status");

        JTextField tf_invCode = new JTextField();
        JTextField tf_serialNo = new JTextField();
        JTextField tf_desc = new JTextField();
        JTextField tf_safetyStock = new JTextField("0");
        JTextField tf_qty = new JTextField("0");
        JComboBox cb_class = new JComboBox();
        function.getClassList(cb_class);
        cb_class.setEditable(true);
        JTextField tf_pin = new JTextField();
        JTextField tf_box = new JTextField();
        JComboBox<String> cb_wp = new JComboBox<>(new String[] {"3k psi" ,"5k psi", "10k psi", "15k psi", "20k psi"});
        cb_wp.setEditable(true);
        JComboBox<String> cb_stdH2S = new JComboBox<>(new String[]{"STD", "H2S", "N/C", "N/A"});
        JComboBox<String> cb_status = new JComboBox<>(new String[]{"Ready", "Repair", "Defective"});

        if(item!=null) {
            oldSerialNo = item.getSerialNo();
            tf_invCode.setText(item.getInventoryCode());
            tf_invCode.setEditable(false);
            tf_invCode.setBorder(blackline);
            tf_serialNo.setText(item.getSerialNo());
            tf_serialNo.setEditable(false);
            tf_serialNo.setBorder(blackline);
            tf_desc.setText(item.getDescription());
            tf_safetyStock.setText(item.getSafetyStock());
            tf_qty.setText(item.getQty());
            tf_qty.setEditable(false);
            tf_qty.setBorder(blackline);
            cb_class.setSelectedItem(item.get_class());
            tf_pin.setText(item.getPin());
            tf_box.setText(item.getBox());
            cb_wp.setSelectedItem(item.getWp());
            cb_stdH2S.setSelectedItem(item.getStdH2s());
            cb_status.setSelectedItem(item.getStatus());
        }

        JButton btn_addItem = new JButton("Add Item");
        if(item != null) {
            btn_addItem.setText("Update Item");
        }

        tf_invCode.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                String inventoryCode = tf_invCode.getText().trim();
                try {
                    ResultSet rs = function_database.getItemTemplate(inventoryCode);
                    rs.last();
                    if(rs.getRow() == 0) {
                        tf_desc.setText("");
                        cb_class.setSelectedItem("DHTOOLS");
                        tf_pin.setText("");
                        tf_box.setText("");
                        cb_wp.setSelectedItem("3k psi");
                        cb_stdH2S.setSelectedItem("STD");
                    }
                    else {
                        tf_desc.setText(rs.getString(3));
                        cb_class.setSelectedItem(rs.getString(5));
                        tf_pin.setText(rs.getString(6));
                        tf_box.setText(rs.getString(7));
                        cb_wp.setSelectedItem(rs.getString(8));
                        cb_stdH2S.setSelectedItem(rs.getString(9));
                    }
                }
                catch (Exception ex) {
                    System.out.println("Error when setTemplate at func frame_addItem : " + ex.getCause());
                }
            }
        });

        String finalOldSerialNo = oldSerialNo;
        btn_addItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int errorCode = 0;

                String invCode = tf_invCode.getText().toUpperCase().trim();
                if(invCode.length()==0) {
                    errorCode = 1;
                }

                String serialNo = tf_serialNo.getText().toUpperCase().trim();
                if(serialNo.length()==0) {
                    errorCode = 1;
                }

                String desc = tf_desc.getText().trim();
                if(desc.length()==0) {
                    errorCode = 1;
                }

                int safetyStock, qty;
                try {
                    safetyStock = Integer.parseInt(tf_safetyStock.getText().trim());
                    qty = Integer.parseInt(tf_qty.getText().trim());
                }
                catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(mainFrame, "Safety Stock or Qty must be number","Error", JOptionPane.ERROR_MESSAGE);
                }
                safetyStock = Integer.parseInt(tf_safetyStock.getText().trim());
                qty = Integer.parseInt(tf_qty.getText().trim());

                String _class = (String) cb_class.getSelectedItem();
                if(_class.length()==0) {
                    errorCode = 1;
                }

                String pin = tf_pin.getText().trim();
                if(pin.length()==0) {
                    errorCode = 1;
                }

                String box = tf_box.getText().trim();
                if(box.length()==0) {
                    errorCode = 1;
                }

                String wp = (String) cb_wp.getSelectedItem();
                if(wp.length()==0) {
                    errorCode = 1;
                }

                String stdH2s = (String) cb_stdH2S.getSelectedItem();
                if(stdH2s.length()==0) {
                    errorCode = 1;
                }

                String status = (String) cb_status.getSelectedItem();
                if(status.length()==0) {
                    errorCode = 1;
                }

                object_item newItem = new object_item(invCode, serialNo, desc, safetyStock, qty, _class, pin, box, wp, stdH2s, status);
                if(errorCode == 0) {
                    if(item == null) {
                        function_database.addItem(newItem);
                    }
                    if(item != null) {
                        function_database.updateItem(newItem, finalOldSerialNo);
                    }
                    function.updateItemMasterList(itemList);
                    mainFrame.dispose();
                }
                if(errorCode == 1) {
                    JOptionPane.showMessageDialog(mainFrame, "All column must not empty","Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_invCode)
                        .addComponent(lbl_serialNo)
                        .addComponent(lbl_desc)
                        .addComponent(lbl_safetyStock)
                        .addComponent(lbl_qty)
                        .addComponent(lbl_class)
                        .addComponent(lbl_pin)
                        .addComponent(lbl_box)
                        .addComponent(lbl_wp)
                        .addComponent(lbl_stdH2S)
                        .addComponent(lbl_status)
                )
                .addGroup(
                        layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(tf_invCode)
                        .addComponent(tf_serialNo)
                        .addComponent(tf_desc)
                        .addComponent(tf_safetyStock)
                        .addComponent(tf_qty)
                        .addComponent(cb_class)
                        .addComponent(tf_pin)
                        .addComponent(tf_box)
                        .addComponent(cb_wp)
                        .addComponent(cb_stdH2S)
                        .addComponent(cb_status)
                        .addComponent(btn_addItem)
                )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_invCode)
                        .addComponent(tf_invCode)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_serialNo)
                        .addComponent(tf_serialNo)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_desc)
                        .addComponent(tf_desc)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_safetyStock)
                        .addComponent(tf_safetyStock)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_qty)
                        .addComponent(tf_qty)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_class)
                        .addComponent(cb_class)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_pin)
                        .addComponent(tf_pin)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_box)
                        .addComponent(tf_box)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_wp)
                        .addComponent(cb_wp)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_stdH2S)
                        .addComponent(cb_stdH2S)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_status)
                        .addComponent(cb_status)
                )

                .addComponent(btn_addItem)
        );
        layout.linkSize(SwingConstants.VERTICAL, lbl_invCode, tf_invCode);
        layout.linkSize(SwingConstants.VERTICAL, lbl_serialNo, tf_serialNo);
        layout.linkSize(SwingConstants.VERTICAL, lbl_desc, tf_desc);
        layout.linkSize(SwingConstants.VERTICAL, lbl_safetyStock, tf_safetyStock);
        layout.linkSize(SwingConstants.VERTICAL, lbl_qty, tf_qty);
        layout.linkSize(SwingConstants.VERTICAL, lbl_class, cb_class);
        layout.linkSize(SwingConstants.VERTICAL, lbl_pin, tf_pin);
        layout.linkSize(SwingConstants.VERTICAL, lbl_box, tf_box);
        layout.linkSize(SwingConstants.VERTICAL, lbl_wp, cb_wp);
        layout.linkSize(SwingConstants.VERTICAL, lbl_stdH2S, cb_stdH2S);
        layout.linkSize(SwingConstants.VERTICAL, lbl_status, cb_status);
    }
    public static void frame_searchItem(DefaultTableModel model) {
        JFrame mainFrame = new JFrame("Search Item");
        mainFrame.setVisible(true);
        mainFrame.setSize(350,170);
        mainFrame.setLayout(new GridLayout(1, 1));

        JPanel mainPnl = new JPanel();
        mainFrame.add(mainPnl);

        GroupLayout layout = new GroupLayout(mainPnl);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        mainPnl.setLayout(layout);

        JLabel lbl_inventoryCode = new JLabel("Inventory Code");
        JLabel lbl_class = new JLabel("Class");
        JLabel lbl_description = new JLabel("Description");

        JTextField tf_inventoryCode = new JTextField();
        JTextField tf_description = new JTextField();
        JComboBox cb_class = new JComboBox();
        function.getClassList(cb_class);

        KeyAdapter keyReleaseListener = new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                String keySearch_inventoryCode = tf_inventoryCode.getText().trim();
                String keySearch_description = tf_description.getText().trim();
                String keySearch_class = (String) cb_class.getSelectedItem();
                function.searchItem(keySearch_inventoryCode, keySearch_description, keySearch_class, model);
            }
        };

        tf_inventoryCode.addKeyListener(keyReleaseListener);
        tf_description.addKeyListener(keyReleaseListener);
        cb_class.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String keySearch_inventoryCode = tf_inventoryCode.getText().trim();
                String keySearch_description = tf_description.getText().trim();
                String keySearch_class = "";
                String selectedClass = (String) cb_class.getSelectedItem();
                if(selectedClass!="All classes") {
                    keySearch_class = selectedClass;
                }
                function.searchItem(keySearch_inventoryCode, keySearch_description, keySearch_class, model);
            }
        });

        JButton btn_close = new JButton("Close");
        btn_close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                mainFrame.dispose();
            }
        });

        JButton btn_resetFilter = new JButton("Reset Filters");
        btn_resetFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                tf_description.setText("");
                tf_inventoryCode.setText("");
                cb_class.setSelectedItem("");
                function.updateItemMasterList(model);
            }
        });

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(
                                layout.createSequentialGroup()
                                .addGroup(
                                        layout.createParallelGroup()
                                        .addComponent(lbl_inventoryCode)
                                        .addComponent(lbl_description)
                                        .addComponent(lbl_class)
                                )
                                .addGroup(
                                        layout.createParallelGroup()
                                        .addComponent(tf_inventoryCode)
                                        .addComponent(tf_description)
                                        .addComponent(cb_class)
                                )
                        )
                        .addGroup(
                                layout.createSequentialGroup()
                                .addComponent(btn_resetFilter)
                                .addComponent(btn_close)
                        )
                )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_inventoryCode)
                        .addComponent(tf_inventoryCode)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_description)
                        .addComponent(tf_description)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_class)
                        .addComponent(cb_class)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(btn_resetFilter)
                        .addComponent(btn_close)
                )
        );

        layout.linkSize(SwingConstants.VERTICAL, lbl_description, tf_description);
        layout.linkSize(SwingConstants.VERTICAL, lbl_class, cb_class);
        layout.linkSize(SwingConstants.VERTICAL, lbl_inventoryCode, tf_inventoryCode);
    }
    public static void frame_moveItem(DefaultTableModel itemList, object_item item) {
        JFrame mainFrame = new JFrame("Move Item");
        mainFrame.setVisible(true);
        mainFrame.setSize(350, 220);
        mainFrame.setLayout(new GridLayout(1, 1));

        JPanel mainPnl = new JPanel();
        GroupLayout layout = new GroupLayout(mainPnl);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        mainPnl.setLayout(layout);
        mainFrame.add(mainPnl);

        JLabel lbl_invCode = new JLabel("Inventory Code");
        JLabel lbl_serialNo = new JLabel("Serial Number");
        JLabel lbl_desc = new JLabel("Description");
        JLabel lbl_project = new JLabel("Project");
        JLabel lbl_qty = new JLabel("Qty");

        JTextField tf_invCode = new JTextField();
        tf_invCode.setText(" " + item.getInventoryCode());
        tf_invCode.setEditable(false);
        tf_invCode.setBorder(blackline);

        JTextField tf_serialNo = new JTextField();
        tf_serialNo.setText(" " + item.getSerialNo());
        tf_serialNo.setEditable(false);
        tf_serialNo.setBorder(blackline);

        JTextField tf_desc = new JTextField();
        tf_desc.setText(" " + item.getDescription());
        tf_desc.setEditable(false);
        tf_desc.setBorder(blackline);

        JComboBox cb_moveTo = new JComboBox();
        function.getProjectList(cb_moveTo);
        JTextField tf_qty = new JTextField();

        JButton btn_moveItem = new JButton("Move Item");
        btn_moveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if( Integer.parseInt(tf_qty.getText().trim()) > Integer.parseInt(item.getQty())) {
                    JOptionPane.showMessageDialog(mainFrame, "Moved qty cannot bigger than current qty", "Qty Error", JOptionPane.ERROR_MESSAGE);
                }
                else {
                    String projectName = (String) cb_moveTo.getSelectedItem();
                    object_projectItem projectItem = new object_projectItem(
                            item.getSerialNo(),
                            Integer.parseInt(tf_qty.getText().trim())
                    );
                    function_database.moveItem(projectName, projectItem, Integer.parseInt(item.getQty()));
                    function.updateItemMasterList(itemList);
                    mainFrame.dispose();
                }
            }
        });

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_invCode)
                        .addComponent(lbl_serialNo)
                        .addComponent(lbl_desc)
                        .addComponent(lbl_project)
                        .addComponent(lbl_qty)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(tf_invCode)
                        .addComponent(tf_serialNo)
                        .addComponent(tf_desc)
                        .addComponent(cb_moveTo)
                        .addComponent(tf_qty)
                        .addComponent(btn_moveItem)
                )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_invCode)
                        .addComponent(tf_invCode)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_serialNo)
                        .addComponent(tf_serialNo)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_desc)
                        .addComponent(tf_desc)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_project)
                        .addComponent(cb_moveTo)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_qty)
                        .addComponent(tf_qty)
                )
                .addComponent(btn_moveItem)
        );
        layout.linkSize(SwingConstants.VERTICAL, lbl_invCode, tf_invCode);
        layout.linkSize(SwingConstants.VERTICAL, lbl_serialNo, tf_serialNo);
        layout.linkSize(SwingConstants.VERTICAL, lbl_desc, tf_desc);
        layout.linkSize(SwingConstants.VERTICAL, lbl_project, cb_moveTo);
        layout.linkSize(SwingConstants.VERTICAL, lbl_qty, tf_qty);
    }
    public static void frame_itemMovement(object_item item) {
        JFrame mainFrame = new JFrame("Item Movement");
        mainFrame.setVisible(true);
        mainFrame.setSize(350, 390);
        mainFrame.setLayout(new GridLayout(1, 1));

        JPanel mainPnl = new JPanel();
        GroupLayout layout = new GroupLayout(mainPnl);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        mainPnl.setLayout(layout);
        mainFrame.add(mainPnl);

        JLabel lbl_invCode = new JLabel("Inventory Code");
        JLabel lbl_serialNo = new JLabel("Serial Number");
        JLabel lbl_desc = new JLabel("Description");

        JTextField tf_invCode = new JTextField();
        tf_invCode.setText(" " + item.getInventoryCode());
        tf_invCode.setEditable(false);
        tf_invCode.setBorder(blackline);

        JTextField tf_serialNo = new JTextField();
        tf_serialNo.setText(" " + item.getSerialNo());
        tf_serialNo.setEditable(false);
        tf_serialNo.setBorder(blackline);

        JTextField tf_desc = new JTextField();
        tf_desc.setText(" " + item.getDescription());
        tf_desc.setEditable(false);
        tf_desc.setBorder(blackline);

        String[] header = {"Date", "Move from", "Move to", "Qty"};
        DefaultTableModel model = new DefaultTableModel(header, 0);
        JTable tabel = new JTable(model);
        tabel.setDefaultEditor(Object.class, null);
        tabel.getColumnModel().getColumn(3).setMaxWidth(50);
        JScrollPane pn = new JScrollPane(tabel);
        function.updateItemMovement(item.getSerialNo(), model);

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(
                                layout.createSequentialGroup()
                                .addGroup(
                                        layout.createParallelGroup()
                                        .addComponent(lbl_invCode)
                                        .addComponent(lbl_serialNo)
                                        .addComponent(lbl_desc)
                                )
                                .addGroup(
                                        layout.createParallelGroup()
                                        .addComponent(tf_invCode)
                                        .addComponent(tf_serialNo)
                                        .addComponent(tf_desc)
                                )
                        )
                        .addComponent(pn)
                )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_invCode)
                        .addComponent(tf_invCode)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_serialNo)
                        .addComponent(tf_serialNo)
                )
                .addGroup(
                        layout.createParallelGroup()
                        .addComponent(lbl_desc)
                        .addComponent(tf_desc)
                )
                .addComponent(pn)
        );
    }
    public static void frame_restockItem(DefaultTableModel itemList, object_item item) {
        JFrame mainFrame = new JFrame("Restock Item");
        mainFrame.setVisible(true);
        mainFrame.setSize(350, 185);
        mainFrame.setLayout(new GridLayout(1, 1));

        JPanel mainPnl = new JPanel();
        GroupLayout layout = new GroupLayout(mainPnl);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        mainPnl.setLayout(layout);
        mainFrame.add(mainPnl);

        JLabel lbl_invCode = new JLabel("Inventory Code");
        JLabel lbl_serialNo = new JLabel("Serial Number");
        JLabel lbl_desc = new JLabel("Description");
        JLabel lbl_qty = new JLabel("Restock Qty");

        JTextField tf_invCode = new JTextField();
        tf_invCode.setText(" " + item.getInventoryCode());
        tf_invCode.setEditable(false);
        tf_invCode.setBorder(blackline);

        JTextField tf_serialNo = new JTextField();
        tf_serialNo.setText(" " + item.getSerialNo());
        tf_serialNo.setEditable(false);
        tf_serialNo.setBorder(blackline);

        JTextField tf_desc = new JTextField();
        tf_desc.setText(" " + item.getDescription());
        tf_desc.setEditable(false);
        tf_desc.setBorder(blackline);

        JTextField tf_qty = new JTextField();

        JButton btn_moveItem = new JButton("Restock Item");
        btn_moveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    int newQty = Integer.parseInt(tf_qty.getText().trim()) + Integer.parseInt(item.getQty());
                    function_database.updateItemStock("item_master", item.getSerialNo(), newQty);
                    function_database.addItemMovement("", "item_master", item.getSerialNo(), Integer.parseInt(tf_qty.getText().trim()));
                    function.updateItemMasterList(itemList);
                    mainFrame.dispose();
                }
                catch (Exception e) {
                    JOptionPane.showMessageDialog(mainFrame, "Qty must be a number", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGroup(
                                layout.createParallelGroup()
                                        .addComponent(lbl_invCode)
                                        .addComponent(lbl_serialNo)
                                        .addComponent(lbl_desc)
                                        .addComponent(lbl_qty)
                        )
                        .addGroup(
                                layout.createParallelGroup()
                                        .addComponent(tf_invCode)
                                        .addComponent(tf_serialNo)
                                        .addComponent(tf_desc)
                                        .addComponent(tf_qty)
                                        .addComponent(btn_moveItem)
                        )
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(
                                layout.createParallelGroup()
                                        .addComponent(lbl_invCode)
                                        .addComponent(tf_invCode)
                        )
                        .addGroup(
                                layout.createParallelGroup()
                                        .addComponent(lbl_serialNo)
                                        .addComponent(tf_serialNo)
                        )
                        .addGroup(
                                layout.createParallelGroup()
                                        .addComponent(lbl_desc)
                                        .addComponent(tf_desc)
                        )
                        .addGroup(
                                layout.createParallelGroup()
                                        .addComponent(lbl_qty)
                                        .addComponent(tf_qty)
                        )
                        .addComponent(btn_moveItem)
        );
        layout.linkSize(SwingConstants.VERTICAL, lbl_invCode, tf_invCode);
        layout.linkSize(SwingConstants.VERTICAL, lbl_serialNo, tf_serialNo);
        layout.linkSize(SwingConstants.VERTICAL, lbl_desc, tf_desc);
        layout.linkSize(SwingConstants.VERTICAL, lbl_qty, tf_qty);
    }
}
